<?php

return array(

    'groups' => array(
        'max_invites_count' => 50,
        'new_groups_daily' => 5,
    ),

    'controllers' => array(
        'invokables' => array(
            'Group\Controller\Index' => 'Group\Controller\IndexController',
        ),
    ),

    'router' => array(
        'routes' => array(

            'group.edit' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/groups/edit/:id/',
                    'constrains' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'edit'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.list' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'list'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.show' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/groups/:id/',
                    'constrains' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'show'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.search' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/search/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'search'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.add' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/add/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'add'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.users' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/groups/:id/users/',
                    'constrains' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'users'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.invite' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/invite/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'invite'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.join' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/join/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'join'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

            'group.unjoin' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/groups/unjoin/',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Index',
                        'action'     => 'unjoin'
                    ),
                ),
                'allow' => array(
                    'ROLE_MEMBER',
                ),
            ),

        )
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../../Application/view',
        ),
    ),

);

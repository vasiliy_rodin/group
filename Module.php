<?php

namespace Group;

use Application\Db\EntityModel;
use Application\Db\Manager;
use Application\Image\ImageService;
use Group\Service\GroupService;
use Group\View\Helper\GroupEventRoute;
use Group\View\Helper\IsEvent;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\ServiceManager\ServiceManager;
use Zend\View\HelperPluginManager;

class Module
{

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function getServiceConfig()
	{
		return array(
			'factories' => array(

                'GroupService' => function(ServiceManager $sm)
                {
                    /** @var Manager $modelManager */
                    $modelManager = $sm->get('ModelManager');
                    $socialService = $sm->get('SocialService');

                    $config = $sm->get('Config');
                    $config = isset($config['groups']) ? $config['groups'] : array();

                    return new GroupService($modelManager, $socialService, $config);
                },

			),
		);
	}

}

<?

namespace Group\Service;

use Application\Controller\Member;
use Application\Db\Manager;

use DateTime;
use Exception;
use Group\Exception\GroupException;
use Group\Model\Group as GroupModel;
use Application\Model\User as UserModel;

use Group\Entity\Group as GroupEntity;
use Application\Entity\User as UserEntity;

use Social\Model\SocialUserCount;
use Social\Service\SocialService;
use Zend\Db\Sql\Predicate\Expression;

class GroupService
{

    const PAGE_USER_COUNT = 20;
    const PAGE_GROUP_COUNT = 10;

    const MESSAGE_NO_GROUP_ID = 'No group id';
    const MESSAGE_INVALID_GROUP_ID = 'Invalid group id';
    const MESSAGE_NOT_FOUND_GROUP = 'Group is not found';
    const MESSAGE_NOT_JOINED_GROUP = 'You`re not joined to the group';
    const MESSAGE_INVITE_LIMIT = 'Invite limit: %s per day';

    public static $model = 'Group';
    public static $counterId = SocialUserCount::GROUPS;
    public static $inviteCounterId = SocialUserCount::GROUP_INVITES;
    public static $dailyCounterId = SocialUserCount::GROUP_DAILY;

    public function __construct(Manager $modelManager, SocialService $socialService, $config)
    {
        $this->modelManager = $modelManager;
        $this->socialService = $socialService;
        $this->config = $config;
    }

    public function getConfigValue($key)
    {
        return isset($this->config[$key]) ? $this->config[$key] : false;
    }

    public function isAllowedToCreate($userId)
    {
        $configKey = 'new_' . strtolower(static::$model) . 's_daily';
        return $this->getSocialService()->getCounterValue($userId, static::$dailyCounterId, true) < $this->getConfigValue($configKey);
    }

    public function create($ownerId, $data)
    {
        if (!$this->isAllowedToCreate($ownerId)) {
            return false;
        }

        $groupId = $this->getGroupModel()->create($ownerId, $data);

        // Update user count
        $this->getGroupModel()->updateUserCount($groupId, $this->getGroupModel()->getUserCount($groupId));

        // Update restrictions for user
        $this->getSocialService()->updateCounter(static::$dailyCounterId, $ownerId, $this->getGroupModel()->getCountByUserId($ownerId));

        return $groupId;
    }

    public function deleteGroup($groupId)
    {
        $userIds = $this->getGroupModel()->findUserIds($groupId);

        $this->getGroupModel()->deleteConnection($groupId, $userIds);
        $this->getGroupModel()->delete(['id' => $groupId]);

        foreach($userIds as $userId) {
            $this->getSocialService()->updateCounter(SocialUserCount::EVENTS, $userId, $this->modelManager->get('Event')->getInvitesCount($userId));
            $this->getSocialService()->updateCounter(SocialUserCount::GROUPS, $userId, $this->modelManager->get('Group')->getInvitesCount($userId));
        }

    }

    public function update($groupId, $data)
    {
        $this->getGroupModel()->edit($groupId, $data);
    }

    public function checkGroupValidity($userId, $groupId, $useGroup = false)
    {
        if (!$groupId) {
            throw new GroupException(self::MESSAGE_NO_GROUP_ID);
        }

        $groupId = (int) $groupId;

        if (!($groupId > 0)) {
            throw new GroupException(self::MESSAGE_INVALID_GROUP_ID);
        }

        $groupModel = $useGroup ? $this->modelManager->get('Group') : $this->getGroupModel();

        /** @var GroupEntity $group */
        $group = $groupModel->find($groupId);
        /** @var UserEntity $user */
        $user = $this->getUserModel()->find($userId);

        if (!$group) {
            throw new GroupException(self::MESSAGE_NOT_FOUND_GROUP);
        }

        $isUserInGroup = $group->getConfirmedStatus($user) == GroupEntity::STATUS_MEMBER;
        $isUserIsOwner = $group->getOwner() ? $group->getOwner()->getId() == $userId : false;

        if (!$isUserInGroup && !$isUserIsOwner) {
            throw new GroupException(self::MESSAGE_NOT_JOINED_GROUP);
        }

        return true;
    }

    public function invite($userId, $groupId, array $userIds)
    {
        $userIds = self::sanitizeUserIds($userIds);

        $userInvited = count($userIds);
        $userInvitedToday = $this->getSocialService()->getCounterValue($userId, static::$inviteCounterId, true);

        // Check new invites
        if (($userInvited + $userInvitedToday) > $this->getConfigValue('max_invites_count')) {
            throw new GroupException(sprintf(self::MESSAGE_INVITE_LIMIT, $this->getConfigValue('max_invites_count')));
        }

        // Invite
        $this->getGroupModel()->invite($userId, $groupId, $userIds);

        // Update counters
        $this->getSocialService()->updateCounter(static::$inviteCounterId, $userId, $this->getGroupModel()->getSentInvitesCount($userId));

        // Make notifications for users
        foreach ($userIds as $userId) {
            $this->getSocialService()->updateCounter(static::$counterId, $userId, $this->getGroupModel()->getInvitesCount($userId));
        }
    }

    public function join($groupId, $userId)
    {
        // Join
        $this->getGroupModel()->join($groupId, $userId);

        // Update counters
        $this->getGroupModel()->updateUserCount($groupId, $this->getGroupModel()->getUserCount($groupId));

        // Update notifications
        $this->getSocialService()->updateCounter(static::$counterId, $userId, $this->getGroupModel()->getInvitesCount($userId));

    }

    public function unjoin($groupId, $userId)
    {
        // Unjoin
        $this->getGroupModel()->unjoin($groupId, $userId);

        // Update counters
        $this->getGroupModel()->updateUserCount($groupId, $this->getGroupModel()->getUserCount($groupId));

        // Update notifications
        $this->getSocialService()->updateCounter(static::$counterId, $userId, $this->getGroupModel()->getInvitesCount($userId));
    }

    public function findByUserId($userId, $divideByConfirmedStatus = false)
    {
        return $this->getGroupModel()->findByUserId($userId, $divideByConfirmedStatus);
    }

    /**
     * @return GroupEntity
     */
    public function findById($id)
    {
        return $this->getGroupModel()->find($id);
    }

    public function findBy($criteria, $offset, $limit, $order)
    {
        return $this->getGroupModel()->_findBy($criteria, $offset, $limit, $order);
    }

    public function isOwner($userId, $groupId)
    {
        $group = $this->getGroupModel()->find($groupId);

        if (!$group) {
            return false;
        }

        if (!$group instanceof GroupEntity) {
            return false;
        }

        return $group->getOwner() && $userId == $group->getOwner()->getId();
    }

    /**
     * @return GroupModel
     */
    protected function getGroupModel()
    {
        return $this->modelManager->get(static::$model);
    }

    /**
     * @return UserModel
     */
    protected function getUserModel()
    {
        return $this->modelManager->get('User');
    }


    /**
     * @return SocialService
     */
    protected function getSocialService()
    {
        return $this->socialService;
    }

    /******************
     * Misc functions *
     ******************/

    /**
     * Sanitize user_id
     */
    public static function sanitizeUserIds($userIds)
    {

        if (!is_array($userIds)) {
            return false;
        }

        $result = array();
        foreach ($userIds as $userId) {
            $userId = (int) $userId;
            if ($userId > 0 && !in_array($userId, $result)) {
                $result[] = $userId;
            }
        }

        return $result;
    }

    /**
     * Date convert
     *
     * @param $date
     * @return null|string
     */
    public static function convertDate($date)
    {
        $dt = DateTime::createFromFormat('d.m.Y H:i', $date);

        if (!$dt) {
            return null;
        }

        try {
            return $dt->format('Y-m-d H:i:s');
        } catch (Exception $e) {
            return null;
        }
    }

}
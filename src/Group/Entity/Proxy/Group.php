<?php

namespace Group\Entity\Proxy;


use Application\Db\EntityCollection;
use Application\Db\Manager;
use Application\Entity\City;
use Application\Entity\User;

use Group\Model\Group as GroupModel;
use Event\Model\Event as EventModel;
use Application\Model\User as UserModel;

class Group extends \Group\Entity\Group
{
    protected $__manager;
    protected $map = array(
        'user_count' => 'userCount',
        'type_id' => 'type'
    );

    public function __construct(Manager $manager)
    {
        parent::__construct();
        $this->__manager = $manager;
    }

    public function getOwner()
    {
        if (!$this->owner instanceof User) {
            $this->owner = $this->__manager->get('User')->find($this->owner);
        }

        return parent::getOwner();
    }

    public function getUsers($offset, $limit)
    {
        if (!$this->users instanceof EntityCollection) {

            /** @var GroupModel $groupModel */
            $groupModel = $this->__manager->get('Group');
            $userIds = $groupModel->findUserIds($this->getId(), $offset, $limit);

            if (empty($userIds)) {
                return new EntityCollection();
            }

            /** @var UserModel $userModel */
            $userModel = $this->__manager->get('User');
            $this->users = $userModel->_findBy(array('id' => $userIds), 0, $limit);
        }

        return parent::getUsers($offset, $limit);
    }

    public function getConfirmedStatus(User $user = null)
    {
        if (!is_numeric($this->confirmed)) {
            /** @var GroupModel $groupModel */
            $groupModel = $this->__manager->get('Group');
            $this->confirmed = $groupModel->checkStatus($this->getId(), $user->getId());
        }

        return parent::getConfirmedStatus($user);
    }

}

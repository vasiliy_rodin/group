<?php

namespace Group\Entity;

use Application\Db\Entity;
use Application\Db\EntityCollection;

use Application\Entity\City;
use Application\Entity\User;
use Application\Image\ImageService;

use Group\Model\Group as GroupModel;

class Group extends Entity
{

    const STATUS_NOT_MEMBER = 0;
    const STATUS_NOT_CONFIRMED = 1;
    const STATUS_MEMBER = 2;

    protected $name;
    protected $type;
    protected $description;
    protected $owner;
    protected $confirmed;
    protected $userCount;

    protected $users;

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get members
     *
     * @param $offset
     * @param $limit
     *
     * @return EntityCollection
     */
    public function getUsers($offset, $limit)
    {
        return $this->users;
    }

    /**
     * Get member count
     */
    public function getUserCount()
    {
        return $this->userCount;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function hasOwner()
    {
        return $this->owner instanceof User || $this->owner > 0;
    }

    public function getConfirmedStatus(User $user = null)
    {
        return $this->confirmed;
    }

    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed ? self::STATUS_MEMBER : self::STATUS_NOT_CONFIRMED;
    }
}
<?php

namespace Group\Model;

use Application\Db\EntityCollection;
use Application\Db\ManagedTable;

use DateTime;
use Group\Entity\Group as GroupEntity;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class Group extends ManagedTable
{

    const IS_CONFIRMED = 1;

    const CUSTOM_GROUP_TYPE_ID = 1;
    const CITY_GROUP_TYPE_ID = 0;

    public static $connectionTable = 'group_user';
    public static $connectionField = 'group_id';

    public function getName()
    {
        return 'group';
    }

    /**
     * Group user list
     *
     * @param int  $userId
     * @param bool $divideByConfirmedStatus
     *
     * @return array|EntityCollection
     */
    public function findByUserId($userId, $divideByConfirmedStatus = false)
    {

        $sql = "SELECT ".static::$connectionField.", confirmed FROM ".static::$connectionTable." WHERE ".static::$connectionTable.".user_id = :user_id";

        $data = $this->db->query($sql, array('user_id' => $userId))->toArray();

        $groupIds = array();
        $confirmed = array();

        foreach ($data as $row) {
            $groupIds[] = $row[static::$connectionField];
            $confirmed[$row[static::$connectionField]] = $row['confirmed'];
        }

        if (empty($groupIds)) {
            return new EntityCollection();
        }

        $groups = $this->_findBy(array('id' => $groupIds), null, null, array('id' => 'DESC'));

        $result = array();
        // Разбиваем по подтвержденным и не подтвержденным
        if ($divideByConfirmedStatus) {
            $result = array(
                'confirmed' => new EntityCollection(),
                'not_confirmed' => new EntityCollection()
            );
        }

        foreach ($groups as $group) {
            /** @var GroupEntity $group */
            if (isset($confirmed[$group->getId()])) {

                if ($confirmed[$group->getId()] == self::IS_CONFIRMED) {
                    $group->setConfirmed(true);

                    if ($divideByConfirmedStatus) {
                        $result['confirmed']->add($group);
                    }
                } else {

                    $group->setConfirmed(false);

                    if ($divideByConfirmedStatus) {
                        $result['not_confirmed']->add($group);
                    }

                }

            }
        }

        return $divideByConfirmedStatus ? $result : $groups;
    }

    public function findUserIds($groupId, $offset = false, $limit = false)
    {

        $offset = $offset !== false ? (int) $offset : false;
        $limit = $limit!== false ? (int) $limit : false;

        $limit = ($offset !== false && $limit != false) ? " LIMIT $offset, $limit" : $limit = false;

        $where = ($limit) ? " AND confirmed = :confirmed":'';
        $condition = array('group_id' => $groupId);
        if($limit) {
            $condition['confirmed'] = 1;
        }

        $sql = "SELECT user_id FROM ".static::$connectionTable." WHERE ".static::$connectionField." = :group_id$where$limit";


        $data = $this->db->query($sql, $condition)->toArray();

        $userIds = array();
        foreach ($data as $row) {
            $userIds[] = $row['user_id'];
        }

        return $userIds;

    }

    public function create($ownerId, $data)
    {
        $groupId = $this->insert(array(
            'name' => $data['name'],
            'description' => $data['description'],
            'owner' => (int) $ownerId,
            'city_id' => (int) isset($data['city_id']) ? $data['city_id'] : 0,
            'type_id' => self::CUSTOM_GROUP_TYPE_ID
        ));

        $this->createConnection($ownerId, $groupId, $ownerId, 1);

        return $groupId;
    }

    public function edit($id, $data)
    {

        parent::update($id, array(
            'name' => $data['name'],
            'description' => $data['description'],
            'city_id' => (int) $data['city_id']
        ));

    }

    public function checkStatus($id, $userId)
    {
        $sql = "SELECT confirmed FROM ".static::$connectionTable.
               " WHERE ".static::$connectionTable.".user_id = :user_id".
               " AND ".static::$connectionField." = :group_id";

        $data = $this->db->query($sql, array('user_id' => $userId, 'group_id' => $id))->toArray();

        // Not member - if have no invitation
        if (!isset($data[0]['confirmed'])) {
            return GroupEntity::STATUS_NOT_MEMBER;
        }

        return ($data[0]['confirmed'] == 0) ? GroupEntity::STATUS_NOT_CONFIRMED : GroupEntity::STATUS_MEMBER;
    }

    public function invite($fromUserId, $id, $userIds)
    {
        if (!is_array($userIds)) {
            return false;
        }

        if (empty($userIds)) {
            return false;
        }

        foreach ($userIds as $userId) {
            if ($this->checkStatus($id, $userId) != GroupEntity::STATUS_MEMBER) {
                $this->createConnection($fromUserId, $id, $userId);
            }
        }

        return true;
    }

    public function join($id, $userId)
    {
        $this->confirmConnection($id, $userId);
    }

    public function unjoin($id, $userId)
    {
        $this->deleteConnection($id, $userId);
    }

    public function updateUserCount($id, $value)
    {
        $this->update($id, array('user_count' => $value));
    }

    public function createConnection($fromUserId, $groupId, $userId, $confirmed = 0)
    {
        $sql = "INSERT INTO ".static::$connectionTable." (".static::$connectionField.", user_id, from_user_id, confirmed)
                VALUES (:id, :user_id, :from_user_id, :confirmed) ON DUPLICATE KEY UPDATE confirmed = :confirmed";

        return $this->getConnectionGateway()->getAdapter()->query($sql, array(
            'id' => $groupId,
            'user_id' => $userId,
            'from_user_id' => $fromUserId,
            'confirmed' => $confirmed
        ));

    }

    public function confirmConnection($groupId, $userId)
    {
        $sql = "INSERT INTO ".static::$connectionTable." (".static::$connectionField.", user_id, confirmed)
                VALUES (:id, :user_id, :confirmed) ON DUPLICATE KEY UPDATE confirmed = :confirmed";

        return $this->getConnectionGateway()->getAdapter()->query($sql, array(
            'confirmed' => 1,
            'id' => $groupId,
            'user_id' => $userId,
        ));
    }

    public function deleteConnection($groupId, $userId)
    {
        return $this->getConnectionGateway()->delete(array(
            'user_id' => $userId,
            static::$connectionField => $groupId,
        ));
    }

    public function getUserCount($id)
    {
        $select = new Select(static::$connectionTable);
        $select->columns(array(new Expression("COUNT(".static::$connectionField.") AS cnt")));
        $select->where(array(static::$connectionField => $id, 'confirmed' => 1));

        $result = $this->getConnectionGateway()->selectWith($select);

        return $result->count() > 0 ? (int) $result->current()->cnt : 0;
    }

    public function getInvitesCount($userId)
    {
        $select = new Select(static::$connectionTable);
        $select->columns(array(new Expression("COUNT(".static::$connectionField.") AS cnt")));
        $select->where(array('user_id' => $userId, 'confirmed' => 0));

        $result = $this->getConnectionGateway()->selectWith($select);

        return $result->count() > 0 ? (int) $result->current()->cnt : 0;
    }

    public function getSentInvitesCount($userId)
    {
        $now = new DateTime();

        $select = new Select(static::$connectionTable);
        $select->columns(array(new Expression("COUNT(".static::$connectionField.") AS cnt")));
        $select->where(array('from_user_id' => $userId, new Operator('created', Operator::OP_GTE, $now->format('Y-m-d 00:00:00'))));

        $result = $this->getConnectionGateway()->selectWith($select);

        return $result->count() > 0 ? (int) $result->current()->cnt : 0;
    }

    public function getCountByUserId($userId)
    {
        $now = new DateTime();

        $select = new Select($this->getName());
        $select->columns(array(new Expression("COUNT(id) AS cnt")));
        $select->where(array('owner' => $userId, new Operator('created', Operator::OP_GTE, $now->format('Y-m-d 00:00:00'))));

        $result = $this->gw->selectWith($select);

        return $result->count() > 0 ? (int) $result->current()->cnt : 0;
    }

    protected function getConnectionGateway()
    {
        return new TableGateway(static::$connectionTable, $this->db);
    }

}

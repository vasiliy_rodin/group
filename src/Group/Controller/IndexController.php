<?

namespace Group\Controller;

use Application\Db\EntityCollection;
use Group\Form\Group as GroupForm;
use Group\Exception\GroupException;
use Group\Service\GroupService;

use Search\Filter\FilterCollection;
use Search\Service\SearchService;
use Zend\Http\PhpEnvironment\Request as HttpRequest;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    const CANT_CREATE_NEW = 'You can`t create more than %s new groups in a day';
    const MESSAGE_NO_ID = 'No id';
    const MESSAGE_OWNER_LEAVE = 'Owner can`t unjoin the group';

    public function listAction()
    {
        $currentUserId = $this->getSecurityService()->getIdentity()->getId();

        $groups = $this->getGroupService()->findByUserId($currentUserId, true);

        return array(
            'groups' => $groups,
        );

    }

    public function showAction()
    {
        $groupId = $this->params()->fromRoute('id');

        if (!$groupId) {
            return $this->redirect()->toRoute('group.list');
        }

        $group = $this->getGroupService()->findById($groupId);

        if (!$group) {
            return $this->notFoundAction();
        }

        $ss = $this->getSecurityService();

        return array(
            'group' => $group,
            'inGroup' => ($group->getConfirmedStatus($this->getSecurityService()->getIdentity()) === 2),
            'writable' => $ss->hasRole($ss::ROLE_PLAYER),
        );

    }

    public function usersAction()
    {

        /** @var HttpRequest $request */
        $request = $this->getRequest();

        $groupId = $this->params()->fromRoute('id');

        if (!$groupId) {
            return $this->redirect()->toRoute('group.list');
        }

        $offset = $request->isXmlHttpRequest() ? $this->params()->fromQuery('offset') : 0;

        if( ! $group = $this->getGroupService()->findById($groupId) ) {
            return $this->notFoundAction();
        }

        $users = $group->getUsers($offset, GroupService::PAGE_USER_COUNT);

        if ($request->isXmlHttpRequest()) {
            $view = new ViewModel(array('results' => $users, 'tab' => 'all'));
            $view->setTerminal(1);
            $view->setTemplate('search/index/includes/users');
            return $view;
        }

        return array(
            'group'  => $group,
            'users' => $users,
            'count' => $group->getUserCount(),
        );

    }

    public function searchAction()
    {

        /** @var HttpRequest $request */
        $request = $this->getRequest();

        $query = $this->params()->fromQuery('query', '');
        $offset = $request->isXmlHttpRequest() ? $this->params()->fromQuery('offset') : 0;
        $typeId = $this->params()->fromQuery('type_id') == 1 ? 1 : 0;

        $filters = new FilterCollection();
        $filters->addSimpleFilter('type_id', $typeId);

        $searchResult = $this->getSearchService()->search($query, $offset, '*', $filters, GroupService::PAGE_GROUP_COUNT, 'user_count', 'DESC');

        if (!empty($searchResult)) {
            $groups = $this->getGroupService()->findBy(array('id' => $searchResult['result']), 0, GroupService::PAGE_GROUP_COUNT, array('user_count' => 'DESC'));
            $count = $searchResult['total'];
        } else {
            $groups = new EntityCollection();
            $count = 0;
        }

        if ($request->isXmlHttpRequest()) {
            $view = new ViewModel(array('groups' => $groups));
            $view->setTerminal(1);
            $view->setTemplate('group/index/includes/list');
            return $view;
        }

        return array(
            'groups' => $groups,
            'count' => $count,
            'query' => $query,
            'typeId' => $typeId
        );

    }

    public function addAction()
    {
        $ss = $this->getSecurityService();

        if (!$ss->hasRole($ss::ROLE_PLAYER)) {
            return $this->redirect()->toRoute('group.list');
        }

        $currentUserId = $ss->getIdentity()->getId();

        /** @var HttpRequest $request */
        $request = $this->getRequest();

        if (!$form = $this->getForm() ) {
            return $this->redirect()->toRoute('group.list');
        }

        /**
         * Check restrictions
         */
        if (!$this->getGroupService()->isAllowedToCreate($currentUserId)) {
            return array(
                'group_create_restrict' => sprintf(self::CANT_CREATE_NEW, $this->getGroupService()->getConfigValue('new_groups_daily'))
            );
        }

        $form->setAttribute('action', $this->url()->fromRoute('group.add'));

        $ret['form'] = $form;

        if (!$request->isPost() ) {
            return $ret;
        }

        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return $ret;
        }

        /**
         * Create group
         */
        $groupId = $this->getGroupService()->create($currentUserId, $form->getData());

        /**
         * Send invitations
         */
        $userIds = $this->params()->fromPost('user_id');
        if ($userIds && is_array($userIds) && !empty($userIds)) {

            try {
                $this->getGroupService()->invite($currentUserId, $groupId, $userIds);
            } catch (GroupException $e) {
                $this->flashMessenger()->addErrorMessage($e->getMessage());
                return $this->redirect()->toRoute('group.edit', array('id' => $groupId));
            }

        }

        /**
         * Redirect to group.show
         */
        return $this->redirect()->toRoute('group.show', array('id' => $groupId));

    }


    public function editAction()
    {
        if($this->game()->getCurrentDay() > $this->game()->getLastDay()) {
            return $this->redirect()->toRoute('group.list');
        }


        $currentUserId = $this->getSecurityService()->getIdentity()->getId();

        $groupId = $this->params()->fromRoute('id');

        if (!$groupId) {
            return $this->redirect()->toRoute('group.list');
        }

        if (!$this->getGroupService()->isOwner($currentUserId, $groupId)) {
            return $this->redirect()->toRoute('group.list');
        }

        /** @var HttpRequest $request */
        $request = $this->getRequest();

        if (!$form = $this->getForm() ) {
            return $this->redirect()->toRoute('group.list');
        }

        $form->setAttribute('action', $this->url()->fromRoute('group.edit', array('id' => $groupId)));
        $form->setData($this->getGroupService()->findById($groupId));

        $ret['form'] = $form;

        if (!$request->isPost() ) {
            return $ret;
        }

        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return $ret;
        }

        $this->getGroupService()->update($groupId, $form->getData());

        /**
         * Send invitations
         */
        $userIds = $this->params()->fromPost('user_id');
        if ($userIds && is_array($userIds) && !empty($userIds)) {

            try {
                $this->getGroupService()->invite($currentUserId, $groupId, $this->params()->fromPost('user_id'));
            } catch (GroupException $e) {
                $ret['errors'] = $e->getMessage();
                return $ret;
            }

        }

        return $this->redirect()->toRoute('group.show', array('id' => $groupId));

    }

    public function inviteAction()
    {

        $currentUserId = $this->getSecurityService()->getIdentity()->getId();

        /** @var HttpRequest $request */
        $request = $this->getRequest();

        // Only post and ajax
        if (!$request->isPost() || !$request->isXmlHttpRequest()) {
            return $this->redirect()->toRoute('group.list');
        }

        // Check group validity
        try {
            $this->getGroupService()->checkGroupValidity($currentUserId, $this->params()->fromPost('group_id'));
        } catch (GroupException $e) {
            return $this->ajaxError($e->getMessage());
        }

        // Invite!
        $userIds = $this->params()->fromPost('user_id');
        if ($userIds && is_array($userIds) && !empty($userIds)) {
            try {
                $this->getGroupService()->invite($currentUserId, $this->params()->fromPost('group_id'), $userIds);
            } catch (GroupException $e) {
                return $this->ajaxError($e->getMessage());
            }
        }

        return $this->ajaxResponse('');
    }

    public function joinAction()
    {
        /** @var HttpRequest $request */
        $request = $this->getRequest();

        // POST && AJAX
        if (!$request->isPost() || !$request->isXmlHttpRequest()) {
            return $this->redirect()->toRoute('group.list');
        }

        $currentUserId = $this->getSecurityService()->getIdentity()->getId();

        $groupId = $this->params()->fromPost('id');

        if (!$groupId) {
            return $this->ajaxError(self::MESSAGE_NO_ID);
        }

        $this->getGroupService()->join($groupId, $currentUserId);

        return $this->ajaxResponse('');

    }


    public function unjoinAction()
    {
        /** @var HttpRequest $request */
        $request = $this->getRequest();

        // POST && AJAX
        if (!$request->isPost()) {
            return $this->redirect()->toRoute('group.list');
        }

        $currentUserId = $this->getSecurityService()->getIdentity()->getId();

        $groupId = $this->params()->fromPost('id');

        if (!$groupId) {
            return $this->ajaxError(self::MESSAGE_NO_ID);
        }

        // Owner checking
        if ($this->getGroupService()->isOwner($currentUserId, $groupId)) {
            return $this->ajaxError(self::MESSAGE_OWNER_LEAVE);
        }

        $this->getGroupService()->unjoin($groupId, $currentUserId);

        return $this->ajaxResponse('');

    }

    /**
     * @return GroupForm
     */
    protected function getForm()
    {
        return new GroupForm();
    }

    /**
     * @return SearchService
     */
    protected function getSearchService()
    {
        return $this->getServiceLocator()->get('SearchService');
    }

    /**
     * @return GroupService
     */
    protected function getGroupService()
    {
        return $this->getServiceLocator()->get('GroupService');
    }

    protected function getSecurityService()
    {
        return $this->getServiceLocator()->get('SecurityService');
    }

    /**
     * Ajax response method
     *
     * @param string    $message
     * @param JsonModel $view
     *
     * @return JsonModel
     */
    protected function ajaxResponse($message, JsonModel $view = null)
    {
        $view = $view ? : new JsonModel();
        $status = 'success';
        $view->setVariable('status', $status);
        $view->setVariable('message', $message);
        return $view;
    }

    /**
     * Ajax error method
     *
     * @param string    $message
     * @param JsonModel $view
     *
     * @return JsonModel
     */
    protected function ajaxError($message, JsonModel $view = null)
    {
        $view = $view ? : new JsonModel();
        $view->setVariable('status', 'error');
        $view->setVariable('message', $message);
        return $view;
    }

}

<?php

namespace Group\Form\Filter;

use Application\Form\Filter\Sanitize;
use Zend\Filter\PregReplace;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Digits;
use Zend\Validator\StringLength;

class Group extends InputFilter
{
    public function __construct()
    {

        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                new StringTrim(),
                new StripTags(),
                new PregReplace(array(
                    'pattern' => '/[^a-zа-яё\s\,\-0-9]/iu',
                    'replacement' => '',
                )),
                new PregReplace(array(
                    'pattern' => '/\s{2,}/',
                    'replacement' => ' ',
                )),
            ),
            'validators' => array(
                new StringLength(array(
                    'min' => 0,
                    'max' => 35
                )),
            )
        ));

        $this->add(array(
            'name' => 'description',
            'required' => true,
            'filters' => array(
                new StringTrim(),
                new StripTags(),
                new Sanitize()
            ),
            'validators' => array(
                new StringLength(array(
                    'min' => 0,
                    'max' => 10000
                )),
            ),
        ));

        $this->add(array(
            'name' => 'user_id[]',
            'required' => false,
            'validators' => array(
                new Digits(),
            ),
        ));

    }
}

<?php

namespace Group\Form;

use Application\Entity\City;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\DateTime;
use Zend\Form\Element\File;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

use Group\Form\Filter\Group as GroupFilter;
use Group\Entity\Group as GroupEntity;

class Group extends Form
{
    public function __construct()
    {

        parent::__construct('group');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(new Text('name', array(
            'label' => 'Group name',
        )));

        $this->add(new Textarea('description', array(
            'label' => 'Description',
        )));

        $this->setInputFilter(new GroupFilter());

        $this->add(new Text('user_id[]', array(
            'label' => 'Invite users'
        )));

    }

    public function setData($group)
    {

        if (!$group instanceof GroupEntity) {

            if ($group['user_id']) {
                $this->setOptions(
                    array_merge($this->getOptions(), array('user_id' => $group['user_id']))
                );
            }

            return parent::setData($group);
        }

        /** @var GroupEntity $group */
        $arr = array(
            'name' => $group->getName(),
            'description' => $group->getDescription(),
        );

        return parent::setData($arr);
    }
}
